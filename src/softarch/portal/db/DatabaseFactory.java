package softarch.portal.db;

import java.util.Properties;

import softarch.portal.db.json.JSONDatabaseFacade;
import softarch.portal.db.sql.SQLDatabaseFacade;
import softarch.portal.db.DatabaseException;

public class DatabaseFactory {
	
	public IDatabaseFacade createFacade(Properties properties) throws DatabaseException {
		String dbType = properties.getProperty("dbType");
		String dbUser = properties.getProperty("dbUser");
		String dbPassword = properties.getProperty("dbPassword");
		String dbUrl = properties.getProperty("dbUrl");
		
		if (dbType.equals("sql")) {
			return new SQLDatabaseFacade(dbUser, dbPassword, dbUrl);
		} else if (dbType.equals("json")) {
			return new JSONDatabaseFacade(dbUrl);
		} else {
			throw new DatabaseException("invalid database type");
		}
	}
}
