package softarch.portal.db.json;

import softarch.portal.data.UserProfile;
import softarch.portal.db.DatabaseException;

import java.io.IOException;

/**
 * This class encapsulates the user database.
 * @author Niels Joncheere
 */
public class UserDatabase extends Database {
	/**
	 * Creates a new user database.
	 * @throws IOException 
	 */
	public UserDatabase(String dbUrl) throws DatabaseException {
		super(dbUrl);
	}

	/**
	 * Inserts a new user profile into the user database.
	 * @throws IOException 
	 */
	public void insert(UserProfile profile) throws DatabaseException {
		String username = profile.getUsername();
		String subscription = profile.getClass().getName();
		this.profiles.put(username,gson.toJson(profile));
		this.subscriptions.put(username,subscription);
		saveDatabase();
	}

	/**
	 * Updates an existing user profile in the user database.
	 * @throws IOException 
	 */
	public void update(UserProfile profile)
		throws DatabaseException {
		insert(profile);
	}

	/**
	 * Returns the user with the specified username.
	 */
	public UserProfile findUser(String username)
		throws DatabaseException {
		if (this.subscriptions.containsKey(username)) {
			String subscription = this.subscriptions.get(username);
			try { 
				return (UserProfile)gson.fromJson(this.profiles.get(username), Class.forName(subscription));
			} catch (ClassNotFoundException e) {
				throw new DatabaseException("invalid class");
			}
		} else {
			throw new DatabaseException("username not found");
		}
		
	}

	/**
	 * Checks whether a user with the specified username exists.
	 */
	public boolean userExists(String username)
		throws DatabaseException {
		return this.subscriptions.containsKey(username);
	}
}
