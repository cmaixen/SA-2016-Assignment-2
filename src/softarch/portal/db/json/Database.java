package softarch.portal.db.json;


import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;



import com.google.gson.Gson;

import softarch.portal.db.DatabaseException;





public class Database {
	protected Gson gson;
	protected String dbUrl;
	protected Map<String,String> profiles;
	protected Map<String,String> subscriptions;

	/**
	 * Creates a new database.
	 * @throws DatabaseException 
	 */
	public Database(String dbUrl) throws DatabaseException {
		this.dbUrl	= dbUrl;
		this.gson = new Gson();
		loadDatabase();
	}
	
	private String profilesUrl() {
		return dbUrl + "/profiles.json"; 
	}
	
	private String subscriptionsUrl() {
		return dbUrl + "/subscriptions.json"; 
	}
	
	
	public void loadDatabase() throws DatabaseException {
		try {
			String JSONprofiles = new String(Files.readAllBytes(Paths.get(profilesUrl())), StandardCharsets.UTF_8);
			String JSONsubscriptions = new String(Files.readAllBytes(Paths.get(subscriptionsUrl())), StandardCharsets.UTF_8);
			this.profiles = gson.fromJson(JSONprofiles, Map.class);
			this.subscriptions = gson.fromJson(JSONsubscriptions, Map.class); 
		} catch (IOException e) {
			throw new DatabaseException("Can not access JSON database");
		}
		
	}
	
	public void saveDatabase() throws DatabaseException {
		try {
			String JSONprofiles = gson.toJson(profiles);
			String JSONsubscriptions = gson.toJson(subscriptions);
			Files.write(Paths.get(profilesUrl()), JSONprofiles.getBytes(StandardCharsets.UTF_8));
			Files.write(Paths.get(subscriptionsUrl()), JSONsubscriptions.getBytes(StandardCharsets.UTF_8));
		} catch (IOException e) {
			throw new DatabaseException("Can not access JSON database");
		}

	}
}
