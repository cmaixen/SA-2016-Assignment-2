<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>cudeso.be | Home Automation</title>

    <!-- Bootstrap -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <link href="bootstrap/dashboard.css" rel="stylesheet">
    <link href="bootstrap/bootstrap-switch.css" rel="stylesheet">
    <script src="bootstrap/bootstrap-switch.js"></script>
	<script src="bootstrap/gauge.min.js"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>

  </head>
  <body>
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/index.py"><img src="cudeso.png" alt="" height="32" align="left" />Home Automation</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="index.py?id=3">Help</a></li>
          </ul>
          <form name="login" class="navbar-form navbar-right" method="POST">
            <input id="login" name="login" type="text" class="form-control" placeholder="Login..."/>
            <input type="submit" value="Submit" />
            <input type="hidden" value="28684614-c2f8-4621-a52a-0a750272548c" name="automation" />
          </form>
        </div>
      </div>
    </nav>
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-2 sidebar">
          <ul class="nav nav-sidebar">
                        <li class="active"><a href="index.py">Fixed meters <span class="sr-only">(current)</span></a></li>
            <li><a href="index.py?id=2">Variable meters</a></li>
        
          </ul>
        </div>

        <div class="col-md-10 col-md-offset-2 ">
          <h2 class="page-header">Fixed meters  - Pressure</h2>

            <div class="row  text-left">
                <div class="col-md-10">
                <div class="alert alert-info" >
                  <span class="glyphicon glyphicon-fire" aria-hidden="true"></span>
                    &nbsp;&nbsp;
                    <strong>F2</strong> Discrete inputs
                </div>
                </div>
            </div>
           <div class="row">            <div class="col-md-1">
              <input disabled  type="checkbox" data-size="mini" name="radio0" class="btnswitch">
            </div>
             <div class="col-md-1">
              <input disabled checked type="checkbox" data-size="mini" name="radio1" class="btnswitch">
            </div>
             <div class="col-md-1">
              <input disabled checked type="checkbox" data-size="mini" name="radio2" class="btnswitch">
            </div>
             <div class="col-md-1">
              <input disabled checked type="checkbox" data-size="mini" name="radio3" class="btnswitch">
            </div>
</div><div class="row">             <div class="col-md-1">
              <input disabled checked type="checkbox" data-size="mini" name="radio4" class="btnswitch">
            </div>
             <div class="col-md-1">
              <input disabled checked type="checkbox" data-size="mini" name="radio5" class="btnswitch">
            </div>
             <div class="col-md-1">
              <input disabled checked type="checkbox" data-size="mini" name="radio6" class="btnswitch">
            </div>
             <div class="col-md-1">
              <input disabled checked type="checkbox" data-size="mini" name="radio7" class="btnswitch">
            </div>
</div>
           <div class="row">
            <br /><br />
           </div>
            <div class="row ">
                <div class="col-md-10">
                <div class="alert alert-info" >
                  <span class="glyphicon glyphicon-fire" aria-hidden="true"></span>
                    &nbsp;&nbsp;
                    <strong>F4</strong> Input Registers
                </div>
                </div>
            </div>
           <div class="row">
                    <div class="col-md-2">
                <canvas id="gauge0" width="120" height="120"
                        data-type="canv-gauge"
                        data-title="Pressure"
                        data-min-value="0"
                        data-max-value="127"
                        data-major-ticks="0 10 20 30 40 50 60 70 80 90 100 110 120 127"
                        data-minor-ticks="2"
                        data-stroke-ticks="true"
                        data-units="P"
                        data-value-format="2.2"
                        data-highlights="0 31 #DB2727, 31 126 #1CB910"
                        data-glow="true"
                        data-onready="Gauge.Collection.get('gauge0').setValue( [0]);"
                    ></canvas>
        </div>
         <div class="col-md-2">
                <canvas id="gauge1" width="120" height="120"
                        data-type="canv-gauge"
                        data-title="Pressure"
                        data-min-value="0"
                        data-max-value="127"
                        data-major-ticks="0 10 20 30 40 50 60 70 80 90 100 110 120 127"
                        data-minor-ticks="2"
                        data-stroke-ticks="true"
                        data-units="P"
                        data-value-format="2.2"
                        data-highlights="0 31 #DB2727, 31 126 #1CB910"
                        data-glow="true"
                        data-onready="Gauge.Collection.get('gauge1').setValue( [43]);"
                    ></canvas>
        </div>
         <div class="col-md-2">
                <canvas id="gauge2" width="120" height="120"
                        data-type="canv-gauge"
                        data-title="Pressure"
                        data-min-value="0"
                        data-max-value="127"
                        data-major-ticks="0 10 20 30 40 50 60 70 80 90 100 110 120 127"
                        data-minor-ticks="2"
                        data-stroke-ticks="true"
                        data-units="P"
                        data-value-format="2.2"
                        data-highlights="0 31 #DB2727, 31 126 #1CB910"
                        data-glow="true"
                        data-onready="Gauge.Collection.get('gauge2').setValue( [111]);"
                    ></canvas>
        </div>
         <div class="col-md-2">
                <canvas id="gauge3" width="120" height="120"
                        data-type="canv-gauge"
                        data-title="Pressure"
                        data-min-value="0"
                        data-max-value="127"
                        data-major-ticks="0 10 20 30 40 50 60 70 80 90 100 110 120 127"
                        data-minor-ticks="2"
                        data-stroke-ticks="true"
                        data-units="P"
                        data-value-format="2.2"
                        data-highlights="0 31 #DB2727, 31 126 #1CB910"
                        data-glow="true"
                        data-onready="Gauge.Collection.get('gauge3').setValue( [100]);"
                    ></canvas>
        </div>

           </div>
           <div class="row">
                    <div class="col-md-2">
                <canvas id="gauge4" width="120" height="120"
                        data-type="canv-gauge"
                        data-title="Pressure"
                        data-min-value="0"
                        data-max-value="127"
                        data-major-ticks="0 10 20 30 40 50 60 70 80 90 100 110 120 127"
                        data-minor-ticks="2"
                        data-stroke-ticks="true"
                        data-units="P"
                        data-value-format="2.2"
                        data-highlights="0 31 #DB2727, 31 126 #1CB910"
                        data-glow="true"
                        data-onready="Gauge.Collection.get('gauge4').setValue( [32]);"
                    ></canvas>
        </div>
         <div class="col-md-2">
                <canvas id="gauge5" width="120" height="120"
                        data-type="canv-gauge"
                        data-title="Pressure"
                        data-min-value="0"
                        data-max-value="127"
                        data-major-ticks="0 10 20 30 40 50 60 70 80 90 100 110 120 127"
                        data-minor-ticks="2"
                        data-stroke-ticks="true"
                        data-units="P"
                        data-value-format="2.2"
                        data-highlights="0 31 #DB2727, 31 126 #1CB910"
                        data-glow="true"
                        data-onready="Gauge.Collection.get('gauge5').setValue( [117]);"
                    ></canvas>
        </div>
         <div class="col-md-2">
                <canvas id="gauge6" width="120" height="120"
                        data-type="canv-gauge"
                        data-title="Pressure"
                        data-min-value="0"
                        data-max-value="127"
                        data-major-ticks="0 10 20 30 40 50 60 70 80 90 100 110 120 127"
                        data-minor-ticks="2"
                        data-stroke-ticks="true"
                        data-units="P"
                        data-value-format="2.2"
                        data-highlights="0 31 #DB2727, 31 126 #1CB910"
                        data-glow="true"
                        data-onready="Gauge.Collection.get('gauge6').setValue( [115]);"
                    ></canvas>
        </div>
         <div class="col-md-2">
                <canvas id="gauge7" width="120" height="120"
                        data-type="canv-gauge"
                        data-title="Pressure"
                        data-min-value="0"
                        data-max-value="127"
                        data-major-ticks="0 10 20 30 40 50 60 70 80 90 100 110 120 127"
                        data-minor-ticks="2"
                        data-stroke-ticks="true"
                        data-units="P"
                        data-value-format="2.2"
                        data-highlights="0 31 #DB2727, 31 126 #1CB910"
                        data-glow="true"
                        data-onready="Gauge.Collection.get('gauge7').setValue( [61]);"
                    ></canvas>
        </div>

           </div>
                       <br /><br /><div class="row  text-left">
                <div class="col-md-10">
                <div class="alert alert-info" >
                    <form method="POST" name="unlock">Unlock :
                        <input id="f4" name="f4" type="text" placeholder="Unlock">
                        <input type="hidden" value="28684614-c2f8-4621-a52a-0a750272548c" name="automation" />
                        <input type="submit" value="Submit" />
                    </form>
                </div>
                </div>
            </div>
    
         </div>
        </div>
      </div>
    </div>
    <script>
        $("[class='btnswitch']").bootstrapSwitch();
    </script>
    </body>
</html>
